$(document).ready(function(){

    var inProgress = false;

    $(window).scroll(function() {

        /* ���� ������ ���� + ������ ��������� ������ ��� ����� ������ ����� ��������� */
        if($(window).scrollTop() + $(window).height() >= $(document).height() - 100 && !inProgress) {

            if (relatedJson.length === 0) {
                return;
            }

            var currentNews = relatedJson.shift();

            $.ajax({
                url: currentNews.url,
                method: 'GET',
                data: {},
                beforeSend: function() {
                    ShowLoading();
                    inProgress = true;}
            }).done(function(data){
                if (data.length > 0) {
                    // append data
                    $('#dle-content').append(data);
                    // get news title and other data
                    // change url
                    // history.pushState({}, 'Title', currentNews.url)
                    inProgress = false;
                    HideLoading();
                }});
        }
    });
});